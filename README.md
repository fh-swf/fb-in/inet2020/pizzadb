# PizzaDB
Eine einfache Demoanwendung für den Umgang mit Datenbanken (mongoDB, PostgreSQL) 
in NodeJS.

## Voraussetzungen
- MongoDB Community 4.2.x
- PostgreSQL 12.3.x
- NodeJS 12.x

## Installation

(Standardwerte sind in der .env Datei zu finden)

1. Installation der Vorraussetzungen.
2. MongoDB Datenbank und Collection erstellen.
3. PostgreSQL Datenbank erstellen.
4. Installieren der Abhängigkeiten mittels npm install im Hauptverzeichnis.
5. Anpassen der .env Datei auf die jeweilige Installation.
6. Starten der Anwendung mit npm start im Hauptverzeichnis.


