// Meta Element, welches Typ der Aufrufe bestimmt
const dbtype = document.getElementsByName('dbtype')[0].content;

let addUrl = "api/addmongo";
let getUrl = "api/getmongo";
let deleteUrl = "api/deletemongo";

// Welche API Aufrufe sollen ausgeführt werden?
switch(dbtype){
    case "mongo":
        addUrl = "api/addmongo";
        getUrl = "api/readmongo";
        deleteUrl = "api/deletemongo";
        break;
    case "postgre":
        addUrl = "api/addsql";
        getUrl = "api/readsql";
        deleteUrl = "api/deletesql";
        break;
    default:
        throw "Invalid Database Type!";
}

// OnLoad Bestellungen auslesen
window.onLoad = getOrders();

/**
 * Fügt eine Bestellung zur übergebenen Liste hinzu.
 * @param pizza Pizza, die hinzugefügt wird.
 * @param list HTML List Element, in welches geschrieben wird.
 */
function addPizzaToList(pizza, list){
    list.innerHTML += `
        <li>
            ${pizza.pizza_name}
            <ul>
                <li>${pizza.del_time} Uhr</li>
                <li>${pizza.extras}</li>
            </ul>
        </li>`;
}

/**
 * Gibt alle aktuellen Bestellungen, aus der vorher ausgewählten getUrl zurück.
 */
function getOrders(){
    fetch(getUrl)
        .then(response => response.json())
        .then(data => {
            // Ladesymbol entfernen
            if(document.getElementById("load_orders")) {
                document.getElementById("load_orders").remove();
            }

            let element = document.getElementById("orders");
            element.innerHTML = "";

            if(!Array.isArray(data) || data.length === 0){
                element.innerHTML = "Keine Bestellungen vorhanden!";
            }
            else {
                // Bestellungen nacheinander einfügen
                for(let pizza in data){
                    addPizzaToList(data[pizza], element);
                }
            }
        }).catch(error => console.log(error));
}

/**
 * Entfernt alle derzeitigen Bestellungen von der ausgewählten deleteURL.
 */
function deleteOrders(){
    fetch(deleteUrl, {
        method: "delete"
    })
        .then(response => {
            // Erfolg
            if(response.status === 200){
                document.getElementById("form_error").innerHTML = "";
                document.getElementById("form_success").innerHTML = "Bestellungen gelöscht!";
            }
            // Fehler
            else {
                document.getElementById("form_success").innerHTML = "";
                document.getElementById("form_error").innerHTML = "Löschen fehlgeschlagen!";
            }
            getOrders();
        })
        .catch(error => { console.log(error) });
}

/**
 * Fügt eine neue Bestellung zur ausgewählten addURL hinzu.
 */
function addOrder(){
    // Daten der Form per FormData erhalten
    let formData = new FormData(document.getElementById("order_form"));
    // Daten der FormData in ein Javascript Object Umwandeln.
    let formDataObject = {};
    formData.forEach((value, key) => {formDataObject[key] = value});
    fetch(addUrl, {
        body: JSON.stringify(formDataObject),
        method: "post",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    })
        .then(response => {
            // Erfolg
            if(response.status === 200){
                document.getElementById("form_error").innerHTML = "";
                document.getElementById("form_success").innerHTML = "Bestellung erfolgreich!";
            }
            // Fehler
            else {
                document.getElementById("form_success").innerHTML = "";
                document.getElementById("form_error").innerHTML = "Bestellung fehlgeschlagen!";
            }
            getOrders();
        })
        .catch(error => { console.log(error) });
}
