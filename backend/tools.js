module.exports = {
    /**
     * Überprüft einen übergebenen Request nach seinem Body auf Korrektheit für ein
     * Hinzufügen von Bestellungen.
     * @param req {Request} Request, der überprüft wird.
     * @returns {boolean} Ist
     */
    checkReqArgs: function(req) {
        let valid = true;

        // Ist der Name valide?
        valid &= (typeof(req.body.pizza_name) === "string" || req.body.pizza_name instanceof String) &&
            req.body.pizza_name.length > 0;
        // Ist die Lieferzeit valide?
        valid &= (typeof(req.body.del_time) === "string" || req.body.del_time instanceof String) &&
            /([0-1][0-9]|2[0-3]):[0-5][0-9]/.test(req.body.del_time);
        // Sind die Sonderangaben valide?
        valid &= typeof(req.body.extras) === "string" || req.body.extras instanceof String;

        return valid;
    }
};