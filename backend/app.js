require('dotenv').config()
const express = require("express");
const bodyParser = require('body-parser');
const mongo = require('mongodb');
const { Sequelize, Model, DataTypes } = require('sequelize');
const app = express();

const tools = require("./tools");

app.use(bodyParser.json());
app.use(express.static("frontend"));

//MONGO DB
const url = process.env.MONGO_DB_URL;
const dbName = process.env.MONGO_DB_DATABASE;
const colName = process.env.MONGO_DB_COLLECTION;
const client = new mongo.MongoClient(url, { useUnifiedTopology: true });
const connection = client.connect();

/**
 * API-Aufruf für das Hinzufügen einer Bestellung in die MongoDB.
 */
app.post("/api/addmongo", function (req, res) {
    // Prüfe Request
    if(tools.checkReqArgs(req)){
        // Erstelle Dokument
        const doc = {
            pizza_name: req.body.pizza_name,
            del_time: req.body.del_time,
            extras: req.body.extras
        };
        // Dokument einfügen
        connection.then(() => {
            client.db(dbName).collection(colName).insertOne(doc)
                .then(() => res.sendStatus(200))
                .catch(() => res.status(400).send({
                    message: 'Not able to add Order!'
                }));
        });
    }
    // Request ungültig
    else {
        res.status(400).send({
            message: 'Invalid Form Data!'
        });
    }
});

/**
 * API-Aufruf für das Lesen aller Bestellungen aus der MongoDB.
 */
app.get("/api/readmongo", function (req, res) {
    //Bestellungen lesen
    connection.then(() => {
        client.db(dbName).collection(colName).find({})
            .toArray()
            .then(items => res.status(200).send(items))
            .catch(() => res.status(400).send({
                message: 'Not able to find Orders!'
            }));
    });
});

/**
 * API-Aufruf für das Löschen aller Bestellungen aus der MongoDB.
 */
app.delete("/api/deletemongo", function (req, res) {
    // Bestellungen löschen.
    connection.then(() => {
        client.db(dbName).collection(colName).deleteMany({})
            .then(() => res.sendStatus(200))
            .catch(() => res.status(400).send({
                message: 'Not able to delete Orders!'
            }));
    });
});

//SEQUELIZE
const sequelize = new Sequelize(process.env.POSTGRE_DB_DATABASE,
                                process.env.POSTGRE_DB_USER,
                                process.env.POSTGRE_DB_PASSWORD, {
    host: process.env.POSTGRE_DB_HOST,
    port: process.env.POSTGRE_DB_PORT,
    dialect: 'postgres'
});

class PizzaDemo extends Model {}
PizzaDemo.init({
    pizza_name: Sequelize.STRING(200),
    del_time: Sequelize.STRING(5),
    extras: Sequelize.TEXT
}, { sequelize, modelName: 'pizza_demo' });
PizzaDemo.sync();

/**
 * API-Aufruf für das Hinzufügen einer Bestellung in die PostgreSQL Datenbank.
 */
app.post("/api/addsql", function (req, res) {
    //Prüfe Request
    if(tools.checkReqArgs(req)){
        // Dokument erstellen
        const doc = {
            pizza_name: req.body.pizza_name,
            del_time: req.body.del_time,
            extras: req.body.extras
        };
        // Objekt des Models erstellen und in Datenbank hinzufügen.
        PizzaDemo.create(doc)
            .then(() => res.sendStatus(200))
            .catch(() => res.status(400).send({
                message: 'Not able to add Order!'
            }));
    }
    //Request ungültig
    else {
        res.status(400).send({
            message: 'Invalid Form Data!'
        });
    }
});

/**
 * API-Aufruf für das Lesen aller Bestellungen aus der PostgreSQL Datenbank.
 */
app.get("/api/readsql", function (req, res) {
    //Bestellungen lesen
    PizzaDemo.findAll({raw: true})
        .then(orders => {
            res.status(200).send(orders);
        })
        .catch((error) => {
            console.log(error);
            res.status(400).send({
                message: 'Not able to find Orders!'
            });
        });
});

/**
 * API-Aufruf für das Löschen aller Bestellungen aus der PostgreSQL Datenbank.
 */
app.delete("/api/deletesql", function (req, res) {
    // Bestellungen löschen
    PizzaDemo.destroy({where: {}})
        .then(() => {
            res.sendStatus(200);
        })
        .catch((error) => {
            console.log(error);
            res.status(400).send({
                message: 'Not able to delete Orders!'
            });
        });
});

app.listen(process.env.NODEJS_PORT, function () {
    console.log("Server running on Port " + process.env.NODEJS_PORT);
});